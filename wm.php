<?php
function logMessage($message, $payload) {
    $logFile = 'mention.log';
    $date = date('Y-m-d H:i:s');
    file_put_contents($logFile, "[$date] $message" . PHP_EOL, FILE_APPEND);
    file_put_contents($logfile, $payload . PHP_EOL, FILE_APPEND);
}

$config = parse_ini_file("config.ini");
$recipient = $config['address'];
$payload = file_get_contents('php://input');
$data = json_decode($payload, true);
// logMessage($payload);

if (json_decode($payload) === null) {
    http_response_code(400);
    logMessage("Invalid JSON payload", $payload);
    exit;
}

if (isset($data["secret"])) {
    if ($data["secret"] !== $config["secret"]) {
        http_response_code(403);
        logMessage("Invalid secret: '" . $data["secret"] . "'.", $payload);
        exit;
    }
} else {
    http_response_code(403);
    logMessage("No secret included...", $payload);
    exit;
}

$subject = 'Entropy Arbitrage Webmention';
$body = json_encode($data, JSON_PRETTY_PRINT);
$headers = 'From: webhook@colagioia.net' . "\r\n" .
           'Reply-To: webhook@colagioia.net' . "\r\n" .
           'X-Mailer: PHP/' . phpversion();

if (mail($recipient, $subject, $body, $headers)) {
    http_response_code(200);
} else {
    http_response_code(500);
    logMessage("Failed to send email");
}
?>

